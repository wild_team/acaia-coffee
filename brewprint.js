(function ( $ ) {
    /**
     * @author Benson 20170421 v1
     */

    function checkD3() {
        return window.d3;
    }


    $.fn.brewprint = function(options) {

        var opts = $.extend( {}, $.fn.brewprint.defaults, options );

        if (!checkD3()) {

        }

        function killPrev(d, lessThen, reserved){
            var tmp = [];

            if(reserved < 0){
                return sd;
            }

            //copy data
            d = d.slice();

            reserved = reserved / opts.fDataTimeUnit;
            lessThen = lessThen || 0;

            var lastKey = 0;
            for(k in d){
                if(d[k] <= lessThen){
                    tmp.push(d[k]);
                }else{
                    break;
                }
                lastKey = parseInt(k);
            }

            tmp = (reserved === 0) ? [] : tmp.splice(-reserved);

            return tmp.concat(d.splice((lastKey+1)));
        }

        function plusOnly(d) {
            var max = 0;
            var tmp = [];
            for(k in d){

                var plus = (d[k] > d[k-1]) ? d[k] - d[k-1] : 0;

                tmp.push(max + plus);

//                        if(d[k] > max){
                max = max + plus;
//                        }
            }

            return tmp;
        }

        function formatTooltip(data, html){
            for(var i = 0; i < data.length; i++){
                html = html.replace('{'+i+'}', data[i]);
            }
            return html;
        }

        function formatData(d){

//                opts.bPlusOnly, opts.iPrevSec
            var fData = [];
            var range = 1/opts.fDataTimeUnit;

            //Column title
            fData.push([
                '',
                opts.sBarTooltipTitle,
                {'type': 'string', 'role': 'tooltip', 'p': {'html': true}},
                opts.sLineTooltipTitle,
                {'type': 'string', 'role': 'tooltip', 'p': {'html': true}}
            ]);

            //hack
            fData.push([-1, 0, '', 0, '']);

            var prevSec = 0;
            var prevW = 0;

            for(k in d){
                if(k%range === 0 || parseInt(k) === (d.length - 1)){
                    var v1 = parseFloat(prevSec.toFixed(1));
                    var v2 = (d[k] - prevW);
                    var v3 = d[k];
                    fData.push([
                        v1,
                        v2,
//                                5,
                        formatTooltip([v1,v2,v3], opts.sBarTooltip),
                        v3,
//                                null,
                        formatTooltip([v1,v2,v3], opts.sLineTooltip)
                    ]);
                    prevSec = k * opts.fDataTimeUnit;
                    prevW = d[k];
                }
            }

            //hack
            fData.push([prevSec, 0, '', prevW - 0.01, '']);

//                    console.table(fData);
            return fData;
        }

        return this.each(function() {
            var target = $(this);
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawVisualization);

            function drawVisualization() {
                // Some raw data (not necessarily accurate)

                if(opts.bDebug) console.log('source Data', opts.aData.toString());

                var fData = killPrev(opts.aData, 0, opts.iPrevSec);
                if(opts.bDebug) console.log('after KillPrev', fData.toString());

                if(opts.bPlusOnly){
                    fData = plusOnly(fData);
                }
                if(opts.bDebug) console.log('after plusOnly', fData.toString());

                fData = formatData(fData);
                if(opts.bDebug) console.table(fData);


                var maxSec = d3.max(fData, function (d, i) {
                    return (i > 0 && i < fData.length - 1) ? d[0] : 0;
                });
                var maxBar = d3.max(fData, function (d, i) {
                    return (i > 0) ? d[1] : 0;
                });
                var maxBarWeighted = parseInt(maxBar * opts.fAxisMaxWeighted);

                var maxLine = d3.max(fData, function (d, i) {
                    return (i > 0) ? d[3] : 0;
                });
                var maxLineWeighted = parseInt(maxLine * opts.fAxisMaxWeighted);

                function hAxisTicks(maxNumber, totaltick) {
                    totaltick --;

                    var tmp = [];
                    for(var i = 0; i <= totaltick; i ++){
                        tmp.push( (maxNumber/totaltick*i).toFixed(0));
                    }

                    return tmp;
                }

                var options = {
                    title: opts.sTitle,
                    legend: { //欄位說明
                        position: 'none'
                    },
                    tooltip: {
                        isHtml: opts.bTooltipUseHtml,
//                                trigger: 'selection'
                    },
                    vAxes: {
                        0: { //bar
                            title: opts.sBarLabel, //軸標題
                            titleTextStyle: {color: opts.cBarLabelColor},
                            viewWindowMode: 'explicit',
                            viewWindow: {
                                max: maxBarWeighted,
                                min: 0
                            },
                            gridlineColor: '#EAEAEA',
                            format:'#',
//                                    curveType:'none',
//                                    interpolateNulls:true,
                            textStyle: {color: opts.cBarLabelColor},
//                                    showTextEvery: 5,
                            ticks: hAxisTicks(maxBarWeighted, opts.iYTicksNumber),
                        },
                        1: { //line
                            gridlines: {
                                color: 'transparent',
                                // count: 10,
                            },
                            viewWindow: {
                                max: maxLineWeighted,
                                min: 0
                            },
                            format:"#",
                            title: opts.sLineLabel, //軸標題,
                            titleTextStyle: {color: opts.cLineLabelColor},
                            baselineColor: '#E9E9E9', //底線顏色
                            textStyle: {color: opts.cLineLabelColor},
                            ticks: hAxisTicks(maxLineWeighted, opts.iYTicksNumber),
//                                    showTextEvery: 5,
//                                    slantedText: true,
//                                    slantedTextAngle: 30
                            
                        },
                    },
//                vAxis: {title: 'g/sec'},
                    hAxis: {
//                    title: 'Sec',
//                         showTextEvery: 5,
                        title: opts.sXLabel,
                        gridlines: {color: 'transparent'},
                        baselineColor: 'transparent',
                        format:"#",
                        ticks: hAxisTicks(maxSec, opts.iXTicksNumber)
                    },

                    stackSeries: true,
                    isStacked : true,
                    seriesDefaults: {

                        rendererOptions: {
                            barPadding: 20,
                            barMargin: 10
                        },
                        pointLabels: {
                            show: true,
                            stackedValue: true
                        }
                    },
                    seriesType: 'bars',
//                bar: {groupWidth: 50},
                    colors: [ opts.cBarColor, opts.cLineColor],
                    series: {
                        1: {type: 'line', targetAxisIndex: 1}
                    },
                    crosshair: { //十字準線
                        color: opts.cCrossHairColor,
                        opacity: 0.8,
                        trigger: 'both' //focus, selection
                    },
                    'chartArea': {'left': opts.iMarginLeft, 'right': opts.iMarginRight, 'bottom': opts.iMarginBottom},
//                            'chartArea': {width:'60%'},
                };

                if(opts.bDebug){
                    try{
                        console.table([
                            ['fData.length',fData.length],
                            ['maxSec',maxSec],
                            ['maxBar',maxBar +'=>'+maxBarWeighted],
                            ['maxLine',maxLine +'=>'+maxLineWeighted],
                            ['hAxisTicks('+maxSec+','+opts.iXTicksNumber+')',options.hAxis.ticks.toString()],
                            ['hAxisTicks('+maxBarWeighted+','+opts.iYTicksNumber+')',options.vAxes[0].ticks.toString()],
                            ['hAxisTicks('+maxLineWeighted+','+opts.iYTicksNumber+')',options.vAxes[1].ticks.toString()],
                        ]);
                    }catch(e){
                        console.log(e);
                    }
                }

                var chart = new google.visualization.ComboChart(target.get(0));

                function fixBar(){
                    var svg = d3.select(target.get(0)).select('svg');
                    var graphW = svg.attr('width');
                    var barW = graphW / fData.length * opts.iBarWidthWeighted;
                    var lastBarKey = fData.length - 2;
                    if(opts.bDebug){
                        console.table([
                            ['svg',svg.attr('width') + 'x' + svg.attr('height')],
                            ['graphW',graphW],
                            ['barW',barW],
                            ['lastBarKey',lastBarKey]
                        ]);
                    }

                    function g(position){
                        return svg
                            .selectAll('g')
                            .filter(function (p1, p2, p3) { return p2 === position });
                    }

                    var YGrid = g(3).selectAll('rect');
                    var Y0Grid = g(5).selectAll('rect');
                    var bars = g(4);
                    var path = g(6);
                    var crosshair = g(7);
                    var tooltip = g(10);
                    var xAxisTicks = g(8);
                    var leftLabel = svg.selectAll('text').select(function (d,i) {
                        if((d3.select(this).text() == opts.sBarLabel)){
                            return this;
                        }
                    });
                    var rightLabel = svg.selectAll('text').select(function (d,i) {
                        if((d3.select(this).text() == opts.sLineLabel)){
                            return this;
                        }
                    });

                    //                        bars.attr('transform', 'translate(-10,0)');
                    //                        path.attr('transform', 'translate(-10,0)');
                    //                        crosshair.attr('transform', 'translate(-10,0)');
                    //                        tooltip.attr('transform', 'translate(-10,0)');
                    YGrid.attr('width', YGrid.attr('width') - barW);
                    Y0Grid.attr('width', YGrid.attr('width') - barW);
                    leftLabel.attr('transform', 'rotate(0)');
                    rightLabel.attr('transform', 'rotate(0)');
                    //                        xAxisTicks.attr('transform', 'translate(-10,0)');

                    bars.selectAll('rect').select(function (d, i) {
                        if(i === lastBarKey){
                            d3.select(this).remove();
                        }
                    });
                    bars.select('rect').remove();

                    //Fix second tick position
                    //                        xAxisTicks.selectAll('text').each(function (d, i) {
                    //                            if(i === 1){
                    //                                d3.select(this).attr('transform', 'translate(10,0)');
                    //                            }
                    //                        });


                    function setBarWidth(w) {
                        w = barW;
                        svg
                            .selectAll('g')
                            .filter(function (p1, p2, p3) { return p2 === 4 })
                            .selectAll('rect').attr('width', w)
                            .attr({'transform':'translate('+(-w/2)+',0)'});
                    }
                    google.visualization.events.addListener(chart, 'onmouseover', function(e) {
                        setBarWidth();
                        //Tooltip position
                        var tooltip = svg
                            .selectAll('g')
                            .filter(function (p1, p2, p3) { return p2 === svg.selectAll('g')[0].length-4 });

                        tooltip.attr('transform', 'translate('+opts.iTooltipPosX+','+opts.iTooltipPosY+')');
                        if((e.row === 0 || e.row === lastBarKey)&& e.column === 3){
                            tooltip
                                .style("opacity", 0)
                                .attr('class', 'tt');
                            crosshair.style("opacity", 0);
                        }else{
                            d3.select('.tt').style("opacity", 1);
                            crosshair.style("opacity", 1);
                        }

                    });
                    google.visualization.events.addListener(chart, 'onmouseout', function(e) {
                        setBarWidth();
                    });
                    google.visualization.events.addListener(chart, 'click', function(e) {
                        setBarWidth();
                    });
                    google.visualization.events.addListener(chart, 'select', function(e) {
                        setBarWidth();
                    });
                    setBarWidth();
                }

                d3.select(window).on('resize', function () {
                    chart.draw(google.visualization.arrayToDataTable(fData), options);
                    fixBar();
                });
                
                chart.draw(google.visualization.arrayToDataTable(fData), options);
                fixBar();
            }

        });
    };


    $.fn.brewprint.defaults = {
        'bDebug': true,
        //global
        'sTitle': 'Coffee Brewprint',
        'iMarginLeft': 100, //or 5%
        'iMarginRight': 100,
        'iMarginBottom': 80,
        'cCrossHairColor': '#3bc',
        'iTooltipPosX': 0,
        'iTooltipPosY': -3,
        'bTooltipUseHtml': false,
        'iTickRange': 5,
        'iXTicksNumber': 6,
        'iYTicksNumber': 5,
        'fAxisMaxWeighted': 1.2,
        'sXLabel': 'sec',
        //bar
        'sBarLabel': '(g/s)',
        'cBarLabelColor': '#505050',
        'iBarWidthWeighted': 0.5,
        'cBarColor': '#3E3E3E',
        'sBarTooltipTitle': 'g/s',
        'sBarTooltip': '[{0}s]\n{1} g/s, {2} g',
        //line
        'sLineLabel': '(g)',
        'cLineLabelColor': '#357DB9',
        'cLineColor': '#0A66AF',
        'sLineTooltipTitle': 'g',
        'sLineTooltip': '[{0}s]\n{1} g/s, {2} g',
        //Data
        'aData': [],
        'bPlusOnly': true,
        'iPrevSec': 3, //off = -1
        'fDataTimeUnit': 0.2
    };


}( jQuery ));